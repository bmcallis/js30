// ## Array Cardio Day 2

const people = [
  { name: 'Wes', year: 1988 },
  { name: 'Kait', year: 1986 },
  { name: 'Irv', year: 1970 },
  { name: 'Lux', year: 2015 },
];
console.table(people);

const comments = [
  { text: 'Love this!', id: 523423 },
  { text: 'Super good', id: 823423 },
  { text: 'You are the best', id: 2039842 },
  { text: 'Ramen is my fav food ever', id: 123523 },
  { text: 'Nice Nice Nice!', id: 542328 },
];
console.table(comments);

// Some and Every Checks
// Array.prototype.some() // is at least one person 19 or older?
const curYear = new Date().getFullYear();
const hasAdult = people.some(person => (curYear - person.year) >= 19);
console.log();
console.log('At least one person 19 or older?', hasAdult);

// Array.prototype.every() // is everyone 19 or older?
const allAdults = people.every(person => (curYear - person.year) >= 19);
console.log();
console.log('Is everyone 19 or older?', allAdults);

// Array.prototype.find()
// Find is like filter, but instead returns just the one you are looking for
// find the comment with the ID of 823423
console.log();
console.log('Comment with id 823423', comments.find(c => c.id === 823423));

// Array.prototype.findIndex()
// Find the comment with this ID
// delete the comment with the ID of 823423
const idx = comments.findIndex(c => c.id === 823423);
const newComments = [
  ...comments.slice(0, idx),
  ...comments.slice(idx + 1),
];
console.log();
console.log('Deleted comment 823428 at index', idx);
console.table(newComments);
