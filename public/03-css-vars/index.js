function onChange({ target }) {
  const name = target.name;
  const value = target.value;
  const unit = target.dataset.sizing || '';

  document.documentElement.style.setProperty(`--${name}`, `${value}${unit}`);
}

document.querySelectorAll('input').forEach(elem => {
  elem.addEventListener('input', onChange);
});
