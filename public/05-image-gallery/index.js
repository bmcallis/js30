document.querySelectorAll('.panel').forEach(panel => {
  panel.addEventListener('click', () => {
    panel.classList.toggle('open');
    const topWord = panel.querySelector('p:first-child');
    topWord.classList.toggle('open-active');
    const bottomWord = panel.querySelector('p:last-child');
    bottomWord.classList.toggle('open-active');
  });
  panel.addEventListener('transitionend', evt => {
    if (evt.propertyName.includes('flex')) { // safari uses 'flex' instead of 'flex-grow'
      panel.classList.toggle('open-active');
    }
  });
});
