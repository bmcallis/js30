const endpoint = 'https://gist.githubusercontent.com/Miserlou/c5cd8364bf9b2420bb29/raw/2bf258763cdddd704f8ffd3ea9a3e81d25e2c6f6/cities.json';

const cities = [];
fetch(endpoint)
  .then(response => response.json())
  .then(data => cities.push(...data));

const suggestions = document.querySelector('.suggestions');
const formatNum = new Intl.NumberFormat().format;

function isMatch(city, term) {
  return city.city.toLowerCase().includes(term) ||
      city.state.toLowerCase().includes(term);
}

function onInput(evt) {
  const term = evt.target.value.toLowerCase();
  suggestions.innerHTML = cities.filter(city => isMatch(city, term)).map(city => {
    const regex = new RegExp(term, 'uig');
    const text = `${city.city}, ${city.state}`;
    const nameText = text.replace(regex, '<span class="hl">$&</span>');

    return `
      <li>
        <span class="name">${nameText}</span>
        <span class="population">${formatNum(city.population)}</span>
      </li>
    `;
  }).join('');
}

document.querySelector('.search').addEventListener('input', onInput);
