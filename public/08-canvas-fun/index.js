const canvas = document.getElementById('draw');
const ctx = canvas.getContext('2d');
canvas.width = window.innerWidth;
canvas.height = window.innerHeight;
ctx.lineJoin = 'round';
ctx.lineCap = 'round';

const drawing = {
  active: false,
  lastX: 0,
  lastY: 0,
  hue: 0,
  width: 10,
  widthDelta: 1,
};

function startDraw(evt) {
  drawing.active = true;
  drawing.lastX = evt.offsetX;
  drawing.lastY = evt.offsetY;
}
function endDraw() {
  drawing.active = false;
}
function draw(evt) {
  if (!drawing.active) {
    return;
  }

  ctx.strokeStyle = `hsl(${drawing.hue}, 100%, 50%)`;
  ctx.lineWidth = drawing.width;
  ctx.beginPath();
  ctx.moveTo(drawing.lastX, drawing.lastY);
  ctx.lineTo(evt.offsetX, evt.offsetY);
  ctx.stroke();

  drawing.lastX = evt.offsetX;
  drawing.lastY = evt.offsetY;
  drawing.hue = (drawing.hue + 1) % 360;

  if (drawing.width === 50) {
    drawing.widthDelta = drawing.widthDelta = -1;
  } else if (drawing.width === 0) {
    drawing.widthDelta = 1;
  }

  drawing.width = drawing.width + drawing.widthDelta;
}

canvas.addEventListener('mousedown', startDraw);
canvas.addEventListener('mouseup', endDraw);
canvas.addEventListener('mousemove', draw);
