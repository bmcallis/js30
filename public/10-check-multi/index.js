const checkboxes = document.querySelectorAll('.inbox input[type=checkbox]');

let lastChecked;
function handleCheck(evt) {
  if (evt.shiftKey && evt.target.checked) {
    let inBetween = false;
    checkboxes.forEach(checkbox => {
      if (checkbox === lastChecked || checkbox === evt.target) {
        inBetween = !inBetween;
      }
      if (inBetween) {
        checkbox.checked = true;
      }
    });
  }

  lastChecked = evt.target;
}

checkboxes.forEach(checkbox => {
  checkbox.addEventListener('click', handleCheck);
});
