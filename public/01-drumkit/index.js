window.onkeypress = evt => {
  const divElement = document.querySelector(`div[data-key="${evt.key}"]`);
  if (divElement) {
    divElement.classList.add('playing');

    const audioElement = document.querySelector(`audio[data-key="${evt.key}"]`);
    if (audioElement) {
      audioElement.currentTime = 0;
      audioElement.play();
    }
  }
};

document.querySelectorAll('.key').forEach(elem => {
  elem.addEventListener('transitionend', evt => {
    if (evt.propertyName === 'transform') {
      evt.target.classList.remove('playing');
    }
  });
});
