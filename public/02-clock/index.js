const hourHand = document.querySelector('.hour-hand');
const minHand = document.querySelector('.min-hand');
const secHand = document.querySelector('.second-hand');

const rotate = (elem, position) => {
  elem.style.transform = `rotate(${position}deg)`;
};

const getHourPosition = hours => hours * 30 + 90;
const getMinPosition = mins => mins * 6 + 90;
const getSecPosition = secs => secs * 6 + 90;

const setHands = date => {
  rotate(hourHand, getHourPosition(date.getHours()));
  rotate(minHand, getMinPosition(date.getMinutes()));
  rotate(secHand, getSecPosition(date.getSeconds()));
};

setInterval(() => setHands(new Date()), 1000);
setHands(new Date());
