window.onload = function onload() {
  const playButton = document.querySelector('.toggle');
  const skipButtons = document.querySelectorAll('.player__button[data-skip]');
  const viewer = document.querySelector('.player__video');
  const scrubber = document.querySelector('.progress');
  const progressBar = document.querySelector('.progress__filled');
  const volumeBar = document.querySelector('input[name="volume"]');
  const playbackRateBar = document.querySelector('input[name="playbackRate"]');

  progressBar.style.flexBasis = '0';
  volumeBar.value = 1;
  playbackRateBar.value = 1;

  function togglePlay() {
    if (viewer.paused) {
      viewer.play();
    } else {
      viewer.pause();
    }
  }

  function updateButton() {
    const text = viewer.paused ? '▶' : '⏸';
    playButton.innerText = text;
  }

  playButton.addEventListener('click', togglePlay);
  viewer.addEventListener('click', togglePlay);
  viewer.addEventListener('play', updateButton);
  viewer.addEventListener('pause', updateButton);

  viewer.addEventListener('timeupdate', () => {
    const percentDone = (viewer.currentTime / viewer.duration) * 100;
    progressBar.style.flexBasis = `${percentDone}%`;
  });

  volumeBar.addEventListener('input', () => {
    viewer.volume = volumeBar.value;
  });

  playbackRateBar.addEventListener('input', () => {
    viewer.playbackRate = playbackRateBar.value;
  });

  skipButtons.forEach(button => {
    button.addEventListener('click', () => {
      const skip = parseInt(button.dataset.skip, 10);
      const newTime = viewer.currentTime + skip;
      viewer.currentTime = Math.max(0, Math.min(viewer.duration, newTime));
    });
  });

  let scrubbing = false;
  function updateScrubber(evt) {
    if (scrubbing || evt.type === 'click') {
      const percent = evt.layerX / scrubber.offsetWidth;
      const time = viewer.duration * percent;
      viewer.currentTime = time;
    }
  }

  scrubber.addEventListener('click', updateScrubber);
  scrubber.addEventListener('mousedown', () => scrubbing = true);
  scrubber.addEventListener('mousemove', updateScrubber);
  scrubber.addEventListener('mouseup', () => scrubbing = false);
};
